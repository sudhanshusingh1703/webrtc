// Server

const express = require('express');
const app = express();

const socket = require('socket.io');

const server = app.listen(8080, ()=>{
    console.log('Server is running');
});


const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
// app.use(express.urlencoded({extended:true}));
// app.use(express.json());

app.set('view engine', 'ejs');
app.set('views', './views');

app.use(express.static('public'));

const userRoute = require('./routes/userRoute');
app.use('/', userRoute);

// socket io working with signalling server
var io = socket(server);
io.on("connection", function(socket){
    console.log("User Connected: " + socket.id);

    socket.on("join", function(roomName){
        var rooms = io.sockets.adapter.rooms;
        //console.log(rooms);

        var room = rooms.get(roomName);
        //console.log(room);
        if(room == undefined)
        {
            socket.join(roomName);
            socket.emit("created");
            console.log("Room created: " + roomName);
        }
        else if(room.size == 1){
            socket.join(roomName);
            socket.emit("joined");
            console.log("Joined Room : " + roomName);
        }
        else{
            socket.emit("full");
            console.log("Room is full as off now");
        }

        console.log(rooms);
    })

    // Signalling Server steps Ready, Candidate, Offer, Answer
    
    // When some user is available and ready to join room
    socket.on("ready", function(roomName){
        console.log("Ready");
        console.log("Ready: " + roomName);
        socket.broadcast.to(roomName).emit("ready");
    });

    //Ice Candidates are exchanged across the instance server
    socket.on("candidate", function(candidate, roomName){
        console.log("Candidate");
        console.log(candidate);
        socket.broadcast.to(roomName).emit("candidate", candidate);
    });

    //SDP description offer has information of the users
    socket.on("offer", function(offer, roomName){
        console.log("Offer");
        console.log(offer);
        socket.broadcast.to(roomName).emit("offer", offer);
    });

    //When user initiated a call and the same call is answered this state is called asnwered
    socket.on("answer", function(answer, roomName){
        console.log("Answer");
        socket.broadcast.to(roomName).emit("answer", answer);
    });

})