var socket = io();

var videoChatForm = document.getElementById('video-chat-form');
var videoChatRooms = document.getElementById('video-chat-rooms');
var joinBtn = document.getElementById('join');
var roomInput = document.getElementById('roomName');
var userVideo = document.getElementById('user-video');
var peerVideo = document.getElementById('peer-video');

var divBtnGroup = document.getElementById('btn-group');
var muteButton = document.getElementById('muteButton');

var muteFlag = false;

var roomName = roomInput.value;

navigator.getUserMedia = navigator.getUserMedia || navigator.webKitGetUserMedia || navigator.mozGetUserMedia; 

var creator = false;

var rtcPeerConnection;
var userStream;

var iceServers = {
    iceServers: [
        { urls: "stun:stun.services.mozilla.com"},
        { urls: "stun:stun.1.google.com:19302"}
        // { urls: "stun:stun.1.google.com:19302"},
        // { urls: "stun:stun.1.google.com:19302"},
        // { urls: "stun:stun.1.google.com:19302"},
        // { urls: "stun:stun.1.google.com:19302"},
        // { urls: "stun:stun.1.google.com:19302"},
        // { urls: "stun:stun.1.google.com:19302"},
        // { urls: "stun:stun.l.google.com:19302"},
        // { urls: "stun:stun1.l.google.com:19302"},
        // { urls: "stun:stun2.l.google.com:19302"},
        // { urls: "stun:stun3.l.google.com:19302"},
        // { urls: "stun:stun4.l.google.com:19302"},
        // { urls: "stun:stun01.sipphone.com"},
        // { urls: "stun:stun.ekiga.net"},
        // { urls: "stun:stun.fwdnet.net"},
        // { urls: "stun:stun.ideasip.com"},
        // { urls: "stun:stun.iptel.org"},
        // { urls: "stun:stun.rixtelecom.se"},
        // { urls: "stun:stun.schlund.de"},
        // { urls: "stun:stunserver.org"},
        // { urls: "stun:stun.softjoys.com"},
        // { urls: "stun:stun.voiparound.com"},
        // { urls: "stun:stun.voipbuster.com"},
        // { urls: "stun:stun.voipstunt.com"},
        // { urls: "stun:stun.voxgratia.org"},
        // { urls: "stun:stun.xten.com"}
    ]
};

joinBtn.addEventListener("click", function(){
    roomName = roomInput.value;
    if(roomName == "")
        alert("Please enter a room name");
    else{

        socket.emit("join", roomName);
        // navigator.getUserMedia = navigator.getUserMedia || navigator.webKitGetUserMedia || navigator.mozGetUserMedia; 


        // navigator.getUserMedia(
        //     {
        //         audio: false,
        //         video:{ width: 1280, height: 720 }
        //     },
        //     function(stream){ // user video stream
        //         videoChatForm.style = "display:none";
        //         userVideo.srcObject = stream;
        //         userVideo.onloadedmetadata = function(e){
        //             userVideo.play();
        //         }
        //     },
        //     function(error){
        //         alert("You can't access Media: " + error);
        //     });
    }
});

muteButton.addEventListener("click", function(){
   muteFlag = !muteFlag 
   if(muteFlag){
        userStream.getTracks()[0].enabled = false;
        muteButton.textContent = 'UnMute';
   }
   else{
        userStream.getTracks()[0].enabled = true;
        muteButton.textContent = 'Mute';
   }
});

socket.on("created", function(){
    creator = true;
    navigator.getUserMedia(
        {
            audio: true,
            video:{ width: 1280, height: 720 }
        },
        function(stream){ // user video stream
            userStream = stream;
            videoChatForm.style = "display:none";
            divBtnGroup.style = "display:flex";
            userVideo.srcObject = stream;
            userVideo.onloadedmetadata = function(e){
                userVideo.play();
            }
        },
        function(error){
            alert("You can't access Media: " + error);
        });
});
socket.on("joined", function(){
    roomName = roomInput.value;
    creator = false;
    navigator.getUserMedia(
        {
            audio: true,
            video:{ width: 500, height: 500 }
        },
        function(stream){ // user video stream
            userStream = stream;
            videoChatForm.style = "display:none";
            divBtnGroup.style = "display:flex";
            userVideo.srcObject = stream;
            userVideo.onloadedmetadata = function(e){
                userVideo.play();
            }

            socket.emit("ready", roomName);
        },
        function(error){
            alert("You can't access Media: " + error);
        });
});
socket.on("full", function(){
    alert("Room is full, you can't join!");
});


socket.on("ready", function(){
    // try
    // {
    console.log("reached ready on client");
    roomName = roomInput.value;
    if(creator){
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = OnIceCandidateFunction;

        // Track
        //rtcPeerConnection.ontrack = OnTrackFunction;
        rtcPeerConnection.ontrack = function(event){
            peerVideo.srcObject = event.streams[0];
            event.str
            peerVideo.onloadedmetadata = function(e){
            peerVideo.play();
            }
        };
        rtcPeerConnection.addTrack(userStream.getTracks()[0], userStream); // ['audio','video'] for audio track
        rtcPeerConnection.addTrack(userStream.getTracks()[1], userStream); // ['audio','video'] for video track

        // Offer
        rtcPeerConnection.createOffer(
            function(offer){
                rtcPeerConnection.setLocalDescription(offer);
                console.log(offer);
                socket.emit("offer", offer, roomName);
            },
            function(error){
                console.log("Error in offer: " + error);
            }

        );
    }
    // }
    // catch(e){
    //     console.log("Exception in Offer: " + e)
    // }

});


socket.on("candidate", function(candidate){
    var iceCandidate = new RTCIceCandidate(candidate);
    rtcPeerConnection.addIceCandidate(iceCandidate);
});

socket.on("offer", function(offer){
    if(!creator){
        rtcPeerConnection = new RTCPeerConnection(iceServers);
        rtcPeerConnection.onicecandidate = OnIceCandidateFunction;

        // Track
        //rtcPeerConnection.ontrack = OnTrackFunction;
        rtcPeerConnection.ontrack = function(event){
            peerVideo.srcObject = event.streams[0];
            event.str
            peerVideo.onloadedmetadata = function(e){
            peerVideo.play();
            }
        };
        rtcPeerConnection.addTrack(userStream.getTracks()[0], userStream); // ['audio','video'] for audio track
        rtcPeerConnection.addTrack(userStream.getTracks()[1], userStream); // ['audio','video'] for video track
        
        // Offer
        
        rtcPeerConnection.setRemoteDescription(offer);
        rtcPeerConnection.createAnswer(
            function(answer){
                rtcPeerConnection.setLocalDescription(answer);
                console.log(answer);
                socket.emit("answer", answer, roomName);
            },
            function(error){
                console.log("Error in offer: " + error);
            }

        );
    }
});

socket.on("answer", function(answer){
    // Get Answer for receiver
    rtcPeerConnection.setRemoteDescription(answer);

});

// Get candidates
function OnIceCandidateFunction(event){
    //roomName = roomInput.value;
    if(event.candidate){
        socket.emit("candidate", event.candidate, roomName)
    }
}

// Get stream from the second user
function OnTrackFunction(event){
    console.log("Hello from OnTrackFunction: " + event);
    // peerVideo.srcObject = event.streams[0];
    // event.str
    // peerVideo.onloadedmetadata = function(e){
    //     peerVideo.play();
    // }
}